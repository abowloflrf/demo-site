import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import Setting from "./views/Setting.vue";
import Explore from "./views/Explore.vue";
import Create from "./views/Create.vue";
import MySpeech from "./views/MySpeech.vue";
import SpeechDetail from "./components/SpeechDetail.vue";
import Outline from "./components/Outline.vue";

import SetLevel from "./components/Settings/SetLevel.vue";
import SetPurpose from "./components/Settings/SetPurpose.vue";
import SetTime from "./components/Settings/SetTime.vue";
import SetQuality from "./components/Settings/SetQuality.vue";
import SetAudience from "./components/Settings/SetAudience.vue";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/login",
            name: "login",
            component: Login
        },
        {
            path: "/create",
            name: "create",
            component: SetLevel
        },
        {
            path: "/setting/purpose",
            component: SetPurpose
        },
        {
            path: "/setting/time",
            component: SetTime
        },
        {
            path: "/setting/quality",
            component: SetQuality
        },
        {
            path: "/setting/audience",
            component: SetAudience
        },
        {
            path: "/explore",
            component: Explore
        },
        {
            path: "/my-speech",
            component: MySpeech
        },
        {
            path: "/my-speech/outline",
            component: Outline
        },
        {
            path: "/my-speech/detail",
            component: SpeechDetail
        }
    ]
});
